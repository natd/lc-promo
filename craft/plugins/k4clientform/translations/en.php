<?php
/**
 * k4 Clientform plugin for Craft CMS
 *
 * k4 Clientform Translation
 *
 * @author    Thomas Bauer, kreisvier communications ag
 * @copyright Copyright (c) 2016 Thomas Bauer, kreisvier communications ag
 * @link      http://www.kreisvier.ch
 * @package   K4Clientform
 * @since     0.9
 */

return array(
    
       'k4Cfrm-addOption' => 'Add Option',
       'k4Cfrm-allFieldsRemoved' => 'All fields were removed.',
       'k4Cfrm-allowSelect' => 'Allow Select',
       'k4Cfrm-autocomplete' => 'Autocomplete',
       'k4Cfrm-button' => 'Button',
       'k4Cfrm-cannotBeEmpty' => 'This field cannot be empty',
       'k4Cfrm-checkboxGroup' => 'Checkbox Group',
       'k4Cfrm-checkbox' => 'Checkbox',
       'k4Cfrm-checkboxes' => 'Checkboxes',
       'k4Cfrm-className' => 'Class',
       'k4Cfrm-clearAllMessage' => 'Are you sure you want to clear all fields?',
       'k4Cfrm-clearAll' => 'Clear',
       'k4Cfrm-close' => 'Close',
       'k4Cfrm-content' => 'Content',
       'k4Cfrm-copy' => 'Copy To Clipboard',
       'k4Cfrm-dateField' => 'Date Field',
       'k4Cfrm-description' => 'Help Text',
       'k4Cfrm-descriptionField' => 'Description',
       'k4Cfrm-devMode' => 'Developer Mode',
       'k4Cfrm-editNames' => 'Edit Names',
       'k4Cfrm-editorTitle' => 'Form Elements',
       'k4Cfrm-editXML' => 'Edit XML',
       'k4Cfrm-fieldVars' => 'Field Variables',
       'k4Cfrm-fieldNonEditable' => 'This field cannot be edited.',
       'k4Cfrm-fieldRemoveWarning' => 'Are you sure you want to remove this field?',
       'k4Cfrm-fileUpload' => 'File Upload',
       'k4Cfrm-formUpdated' => 'Form Updated',
       'k4Cfrm-getStarted' => 'Drag a field from the right to this area',
       'k4Cfrm-header' => 'Header',
       'k4Cfrm-hide' => 'Edit',
       'k4Cfrm-hidden' => 'Hidden Input',
       'k4Cfrm-label' => 'Label',
       'k4Cfrm-labelEmpty' => 'Field Label cannot be empty',
       'k4Cfrm-limitRole' => 'Limit access to one or more of the following roles:',
       'k4Cfrm-mandatory' => 'Mandatory',
       'k4Cfrm-maxlength' => 'Max Length',
       'k4Cfrm-minOptionMessage' => 'This field requires a minimum of 2 options',
       'k4Cfrm-name' => 'Name',
       'k4Cfrm-no' => 'No',
       'k4Cfrm-off' => 'Off',
       'k4Cfrm-on' => 'On',
       'k4Cfrm-option' => 'Option',
       'k4Cfrm-optional' => 'optional',
       'k4Cfrm-optionLabelPlaceholder' => 'Label',
       'k4Cfrm-optionValuePlaceholder' => 'Value',
       'k4Cfrm-optionEmpty' => 'Option value required',
       'k4Cfrm-paragraph' => 'Paragraph',
       'k4Cfrm-placeholder' => 'Placeholder',
       'k4Cfrm-placeholders' => "{
           value : 'Value',
           label : 'Label',
           text : '',
           textarea : '',
           email : 'Enter you email',
           placeholder : '',
           className : 'space separated classes',
           password : 'Enter your password'
       }",
       'k4Cfrm-preview' => 'Preview',
       'k4Cfrm-radioGroup' => 'Radio Group',
       'k4Cfrm-radio' => 'Radio',
       'k4Cfrm-removeMessage' => 'Remove Element',
       'k4Cfrm-remove' => '&#215;',
       'k4Cfrm-required' => 'Required',
       'k4Cfrm-richText' => 'Rich Text Editor',
       'k4Cfrm-roles' => 'Access',
       'k4Cfrm-save' => 'Save',
       'k4Cfrm-selectOptions' => 'Options',
       'k4Cfrm-select' => 'Select',
       'k4Cfrm-selectColor' => 'Select Color',
       'k4Cfrm-selectionsMessage' => 'Allow Multiple Selections',
       'k4Cfrm-size' => 'Size',
       'k4Cfrm-sizes' => "{
            xs : 'Extra Small',
            sm : 'Small',
            m : 'Default',
            lg : 'Large'
       }",
       'k4Cfrm-style' => 'Style',
       'k4Cfrm-styles' => "{
        btn: {
          default : 'Default',
          danger : 'Danger',
          info : 'Info',
          primary : 'Primary',
          success : 'Success',
          warning : 'Warning'
        }
       }",
       'k4Cfrm-subtype' => 'Type',
       'k4Cfrm-subtypes' => "{
           text: [
             'text',
             'password',
             'email',
             'color'
           ],
           button: [
             'button',
             'submit'
           ],
           header: [
             'h1',
             'h2',
             'h3'
           ],
           paragraph: [
             'p',
             'address',
             'blockquote',
             'canvas',
             'output'
           ]
       }",
       'k4Cfrm-text' => 'Text Field',
       'k4Cfrm-text' => 'Text Field',
       'k4Cfrm-textArea' => 'Text Area',
       'k4Cfrm-toggle' => 'Toggle',
       'k4Cfrm-warning' => 'Warning!',
       'k4Cfrm-viewXML' => '&lt;/&gt;',
       'k4Cfrm-yes' => 'Yes'
);
