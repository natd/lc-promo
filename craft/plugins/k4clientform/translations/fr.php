<?php
/**
 * k4 Clientform plugin for Craft CMS
 *
 * k4 Clientform Translation
 *
 * @author    Thomas Bauer, kreisvier communications ag
 * @copyright Copyright (c) 2016 Thomas Bauer, kreisvier communications ag
 * @link      http://www.kreisvier.ch
 * @package   K4Clientform
 * @since     0.9
 */

return array(
    
    
    "k4Cfrm-addOption"=>"Ajout d\'une option",
"k4Cfrm-cannotBeEmpty"=>"Ce champ ne peut pas être vide.",
"k4Cfrm-checkbox"=>"case à cocher",
"k4Cfrm-checkboxes"=>"checkboxes",
"k4Cfrm-clearAll"=>"effacement",
"k4Cfrm-close"=>"fermer",
"k4Cfrm-content"=>"teneur",
"k4Cfrm-description"=>"Le texte d\'aide",
"k4Cfrm-descriptionField"=>"description",
"k4Cfrm-editorTitle"=>"éléments de formulaire",
"k4Cfrm-fieldNonEditable"=>"Ce champ ne peut être édité.",
"k4Cfrm-formUpdated"=>"Formulaire mis à jour",
"k4Cfrm-getStarted"=>"Faites glisser les champs de formulaire requis dans ce domaine",
"k4Cfrm-header"=>"titre",
"k4Cfrm-hide"=>"éditer",
"k4Cfrm-hidden"=>"entrée cachée",
"k4Cfrm-label"=>"nom",
"k4Cfrm-labelEmpty"=>"Le nom de champ ne peut pas être vide.",
"k4Cfrm-mandatory"=>"irrésistible",
"k4Cfrm-maxlength"=>"longueur maximale",
"k4Cfrm-minOptionMessage"=>"Ce champ nécessite au moins deux options.",
"k4Cfrm-name"=>"nom",
"k4Cfrm-no"=>"aucun",
"k4Cfrm-off"=>"à partir de",
"k4Cfrm-on"=>"sur",
"k4Cfrm-option"=>"option",
"k4Cfrm-optional"=>"optionnel",
"k4Cfrm-optionLabelPlaceholder"=>"nom",
"k4Cfrm-optionValuePlaceholder"=>"valeur",
"k4Cfrm-optionEmpty"=>"Option est obligatoire",
"k4Cfrm-paragraph"=>"paragraphe",
"k4Cfrm-placeholder"=>"espace réservé",
'k4Cfrm-placeholders' => "{           value : 'value',           label : 'label',           text : '',           textarea : '',           email : 'S'il vous plaît entrez email',           placeholder : '',           className : 'de la barre d'espace pour utiliser plusieurs classes',           password : 'entrer le mot de passe' }",  
"k4Cfrm-preview"=>"avant-première",
"k4Cfrm-radioGroup"=>"boutons radio",
"k4Cfrm-removeMessage"=>"Supprimer membre",
"k4Cfrm-remove"=>"×",
"k4Cfrm-required"=>"irrésistible",
"k4Cfrm-roles"=>"accès",
"k4Cfrm-save"=>"sauvegarder",
"k4Cfrm-selectOptions"=>"Options",
"k4Cfrm-select"=>"sélection",
"k4Cfrm-selectionsMessage"=>"Autoriser la sélection multiple",
"k4Cfrm-subtype"=>"grade",
"k4Cfrm-text"=>"textbox",
"k4Cfrm-textArea"=>"Multiligne Champ de texte",
"k4Cfrm-toggle"=>"sélectionné",
"k4Cfrm-warning"=>"Attention!",
"k4Cfrm-yes"=>"oui",

'k4Cfrm-submitButton' => 'envoyer'
    
);
