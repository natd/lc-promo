<?php
/**
 * k4 Clientform plugin for Craft CMS
 *
 * k4 Clientform Translation
 *
 * @author    Thomas Bauer, kreisvier communications ag
 * @copyright Copyright (c) 2016 Thomas Bauer, kreisvier communications ag
 * @link      http://www.kreisvier.ch
 * @package   K4Clientform
 * @since     0.9
 */

return array(
       'k4Cfrm-eMailSender' => 'E-Mail sender address',
       'k4Cfrm-eMailRecipient' => 'E-Mail recipient address',
       'k4Cfrm-eMailSubject' => 'E-Mail subject',
       'k4Cfrm-eMailTemplate' => 'E-Mail template',
       'k4Cfrm-eMailErrorMsg' => 'Form Error message',
       'k4Cfrm-eMailSuccessMsg' => 'Form Success message',
       'k4Cfrm-eMailHoneyPot' => 'Honey pot fieldname',
    
       'k4Cfrm-eMailSenderHelp' => 'Define your default sender address.',
       'k4Cfrm-eMailRecipientHelp' => 'Define your default recipient address.',
       'k4Cfrm-eMailSubjectHelp' => 'Define your default E-Mail subject.',
       'k4Cfrm-eMailTemplateHelp' => 'Define your default E-Mailtemplate. Use {form} Tag for the form content.',
       'k4Cfrm-eMailErrorMsgHelp' => 'Define your error message.',
       'k4Cfrm-eMailSuccessMsgHelp' => 'Define your success message.',
       'k4Cfrm-eMailHoneyPotHelp' => 'Define the name of your honey pot field.', 
    
    
    
    
       'k4Cfrm-addOption' => 'Option hinzufügen',
       'k4Cfrm-allFieldsRemoved' => 'Alle Felder werden gelöscht.',
       'k4Cfrm-allowSelect' => 'Erlaube Select',
       'k4Cfrm-autocomplete' => 'Autoverfollständigen',
       'k4Cfrm-button' => 'Button',
       'k4Cfrm-cannotBeEmpty' => 'Dieses Feld kann nicht leer sein.',
       'k4Cfrm-checkboxGroup' => 'Checkbox Group',
       'k4Cfrm-checkbox' => 'Checkbox',
       'k4Cfrm-checkboxes' => 'Checkboxes',
       'k4Cfrm-className' => 'Class',
       'k4Cfrm-clearAllMessage' => 'Sind Sie sicher, dass Sie alle Felder löschen wollen?',
       'k4Cfrm-clearAll' => 'Löschen',
       'k4Cfrm-close' => 'Schliessen',
       'k4Cfrm-content' => 'Inhalt',
       'k4Cfrm-copy' => 'In die Zwischenablage kopieren',
       'k4Cfrm-dateField' => 'Datumsfeld',
       'k4Cfrm-description' => 'Hilfetext',
       'k4Cfrm-descriptionField' => 'Beschreibung',
       'k4Cfrm-devMode' => 'Developer Mode',
       'k4Cfrm-editNames' => 'Edit Names',
       'k4Cfrm-editorTitle' => 'Formular Elemente',
       'k4Cfrm-editXML' => 'Edit XML',
       'k4Cfrm-fieldVars' => 'Feld Variablen',
       'k4Cfrm-fieldNonEditable' => 'Dieses Feld kann nicht bearbeitet werden.',
       'k4Cfrm-fieldRemoveWarning' => 'Wollen Sie dieses Feld entfernen?',
       'k4Cfrm-fileUpload' => 'Dateiupload',
       'k4Cfrm-formUpdated' => 'Formular aktualisiert',
       'k4Cfrm-getStarted' => 'Ziehen Sie die gewünschten Formularfelder in diesen Bereich',
       'k4Cfrm-header' => 'Überschrift',
       'k4Cfrm-hide' => 'Bearbeiten',
       'k4Cfrm-hidden' => 'Versteckter Input',
       'k4Cfrm-label' => 'Name',
       'k4Cfrm-labelEmpty' => 'Die Feldbezeichnung kann nicht leer sein.',
       'k4Cfrm-limitRole' => 'Limit access to one or more of the following roles:',
       'k4Cfrm-mandatory' => 'Zwingend',
       'k4Cfrm-maxlength' => 'Maximale Länge',
       'k4Cfrm-minOptionMessage' => 'Dieses Feld benötigt mindestens zwei Optionen.',
       'k4Cfrm-name' => 'Name',
       'k4Cfrm-no' => 'Nein',
       'k4Cfrm-off' => 'Aus',
       'k4Cfrm-on' => 'An',
       'k4Cfrm-option' => 'Option',
       'k4Cfrm-optional' => 'optional',
       'k4Cfrm-optionLabelPlaceholder' => 'Name',
       'k4Cfrm-optionValuePlaceholder' => 'Wert',
       'k4Cfrm-optionEmpty' => 'Option ist zwingend',
       'k4Cfrm-paragraph' => 'Absatz',
       'k4Cfrm-placeholder' => 'Platzhalter',
       'k4Cfrm-placeholders' => "{
           value : 'Wert',
           label : 'Bezeichnung',
           text : '',
           textarea : '',
           email : 'Bitte E-Mail eingeben',
           placeholder : '',
           className : 'leertaste um mehrere Klassen zu verwenden',
           password : 'Passwort eingeben'
       }",
       'k4Cfrm-preview' => 'Vorschau',
       'k4Cfrm-radioGroup' => 'Optionsfelder',
       'k4Cfrm-radio' => 'Radio',
       'k4Cfrm-removeMessage' => 'Entferne Element',
       'k4Cfrm-remove' => '&#215;',
       'k4Cfrm-required' => 'Zwingend',
       'k4Cfrm-richText' => 'Rich Text Editor',
       'k4Cfrm-roles' => 'Zugriff',
       'k4Cfrm-save' => 'Speichern',
       'k4Cfrm-selectOptions' => 'Optionen',
       'k4Cfrm-select' => 'Auswahl',
       'k4Cfrm-selectColor' => 'Farbauswahl',
       'k4Cfrm-selectionsMessage' => 'Mehrfache Auswahl zulassen',
       'k4Cfrm-size' => 'Grösse',
       'k4Cfrm-sizes' => "{
            xs : 'Extra Klein',
            sm : 'Klein',
            m : 'Standard',
            lg : 'Gross'
       }",
       'k4Cfrm-style' => 'Style',
       'k4Cfrm-styles' => "{
        btn: {
          default : 'Default',
          danger : 'Danger',
          info : 'Info',
          primary : 'Primary',
          success : 'Success',
          warning : 'Warning'
        }
       }",
       'k4Cfrm-subtype' => 'Type',
       'k4Cfrm-subtypes' => "{
           text: [
             'text',
             'password',
             'email',
             'color'
           ],
           button: [
             'button',
             'submit'
           ],
           header: [
             'h1',
             'h2',
             'h3'
           ],
           paragraph: [
             'p',
             'address',
             'blockquote',
             'canvas',
             'output'
           ]
       }",
       'k4Cfrm-text' => 'Textfeld',
       'k4Cfrm-textArea' => 'Mehrzeiliges Textfeld',
       'k4Cfrm-toggle' => 'Ausgewählt',
       'k4Cfrm-warning' => 'Achtung!',
       'k4Cfrm-viewXML' => '&lt;/&gt;',
       'k4Cfrm-yes' => 'Ja',
        
       'k4Cfrm-submitButton' => 'inviare'
);
