<?php
/**
 * k4 Clientform plugin for Craft CMS
 *
 * K4Clientform_K4Clientform FieldType
 *
 * --snip--
 * Whenever someone creates a new field in Craft, they must specify what type of field it is. The system comes with
 * a handful of field types baked in, and we’ve made it extremely easy for plugins to add new ones.
 *
 * https://craftcms.com/docs/plugins/field-types
 * --snip--
 *
 * @author    Thomas Bauer, kreisvier communications ag
 * @copyright Copyright (c) 2016 Thomas Bauer, kreisvier communications ag
 * @link      http://www.kreisvier.ch
 * @package   K4Clientform
 * @since     0.9
 */

namespace Craft;

class K4Clientform_K4ClientformFieldType extends BaseFieldType
{
    /**
     * Returns the name of the fieldtype.
     *
     * @return mixed
     */
    public function getName()
    {
        return Craft::t('k4Clientform');
    }

    /**
     * Returns the content attribute config.
     *
     * @return mixed
     */
    public function defineContentAttribute()
    {
        return AttributeType::Mixed;
    }

    public function enhancedTranlate($key){
        if ($key == Craft::t($key)){
          return Craft::t($key,null,null,"en");
        }
        else {
            return Craft::t($key);
        }
    }
    
    /**
     * Returns the field's input HTML.
     *
     * @param string $name
     * @param mixed  $value
     * @return string
     */
    public function getInputHtml($name, $value)
    {
        if (!$value)
            $value = new K4Clientform_K4ClientformModel();

        $id = craft()->templates->formatInputId($name);
        $namespacedId = craft()->templates->namespaceInputId($id);

        /* -- Include our Javascript & CSS */

        craft()->templates->includeCssResource('k4clientform/css/fields/K4Clientform_K4ClientformFieldType.css');
        
        craft()->templates->includeCssResource('k4clientform/css/fields/form-builder.min.css');
        
        
        craft()->templates->includeJsResource('k4clientform/js/fields/K4Clientform_K4ClientformFieldType.js');
        craft()->templates->includeJsResource('k4clientform/js/fields/jquery-ui.min.js');
        craft()->templates->includeJsResource('k4clientform/js/fields/jquery.ui.touch-punch.min.js');
        craft()->templates->includeJsResource('k4clientform/js/fields/form-builder.min.js');
        craft()->templates->includeJsResource('k4clientform/js/fields/form-render.min.js');

        /* -- Variables to pass down to our field.js */

        $jsonVars = array(
            'id' => $id,
            'name' => $name,
            'namespace' => $namespacedId,
            'prefix' => craft()->templates->namespaceInputId(""),
            );

        $jsonVars = json_encode($jsonVars);
        craft()->templates->includeJs("$('#{$namespacedId}').K4Clientform_K4ClientformFieldType(" . $jsonVars . ");");

     
   craft()->templates->includeJs("var languages = { default: {"
. "addOption:'".$this->enhancedTranlate('k4Cfrm-addOption' )."',"
. "allFieldsRemoved:'".$this->enhancedTranlate('k4Cfrm-allFieldsRemoved' )."',"
. "allowSelect:'".$this->enhancedTranlate('k4Cfrm-allowSelect' )."',"
. "autocomplete:'".$this->enhancedTranlate('k4Cfrm-autocomplete' )."',"
. "button:'".$this->enhancedTranlate('k4Cfrm-button' )."',"
. "cannotBeEmpty:'".$this->enhancedTranlate('k4Cfrm-cannotBeEmpty' )."',"
. "checkboxGroup:'".$this->enhancedTranlate('k4Cfrm-checkboxGroup' )."',"
. "checkbox:'".$this->enhancedTranlate('k4Cfrm-checkbox' )."',"
. "checkboxes:'".$this->enhancedTranlate('k4Cfrm-checkboxes' )."',"
. "className:'".$this->enhancedTranlate('k4Cfrm-className' )."',"
. "clearAllMessage:'".$this->enhancedTranlate('k4Cfrm-clearAllMessage' )."',"
. "clearAll:'".$this->enhancedTranlate('k4Cfrm-clearAll' )."',"
. "close:'".$this->enhancedTranlate('k4Cfrm-close' )."',"
. "content:'".$this->enhancedTranlate('k4Cfrm-content' )."',"
. "copy:'".$this->enhancedTranlate('k4Cfrm-copy' )."',"
. "dateField:'".$this->enhancedTranlate('k4Cfrm-dateField' )."',"
. "description:'".$this->enhancedTranlate('k4Cfrm-description' )."',"
. "descriptionField:'".$this->enhancedTranlate('k4Cfrm-descriptionField' )."',"
. "devMode:'".$this->enhancedTranlate('k4Cfrm-devMode' )."',"
. "editNames:'".$this->enhancedTranlate('k4Cfrm-editNames' )."',"
. "editorTitle:'".$this->enhancedTranlate('k4Cfrm-editorTitle' )."',"
. "editXML:'".$this->enhancedTranlate('k4Cfrm-editXML' )."',"
. "fieldVars:'".$this->enhancedTranlate('k4Cfrm-fieldVars' )."',"
. "fieldNonEditable:'".$this->enhancedTranlate('k4Cfrm-fieldNonEditable' )."',"
. "fieldRemoveWarning:'".$this->enhancedTranlate('k4Cfrm-fieldRemoveWarning' )."',"
. "fileUpload:'".$this->enhancedTranlate('k4Cfrm-fileUpload' )."',"
. "formUpdated:'".$this->enhancedTranlate('k4Cfrm-formUpdated' )."',"
. "getStarted:'".$this->enhancedTranlate('k4Cfrm-getStarted' )."',"
. "header:'".$this->enhancedTranlate('k4Cfrm-header' )."',"
. "hide:'".$this->enhancedTranlate('k4Cfrm-hide' )."',"
. "hidden:'".$this->enhancedTranlate('k4Cfrm-hidden' )."',"
. "label:'".$this->enhancedTranlate('k4Cfrm-label' )."',"
. "labelEmpty:'".$this->enhancedTranlate('k4Cfrm-labelEmpty' )."',"
. "limitRole:'".$this->enhancedTranlate('k4Cfrm-limitRole' )."',"
. "mandatory:'".$this->enhancedTranlate('k4Cfrm-mandatory' )."',"
. "maxlength:'".$this->enhancedTranlate('k4Cfrm-maxlength' )."',"
. "minOptionMessage:'".$this->enhancedTranlate('k4Cfrm-minOptionMessage' )."',"
. "name:'".$this->enhancedTranlate('k4Cfrm-name' )."',"
. "no:'".$this->enhancedTranlate('k4Cfrm-no' )."',"
. "off:'".$this->enhancedTranlate('k4Cfrm-off' )."',"
. "on:'".$this->enhancedTranlate('k4Cfrm-on' )."',"
. "option:'".$this->enhancedTranlate('k4Cfrm-option' )."',"
. "optional:'".$this->enhancedTranlate('k4Cfrm-optional' )."',"
. "optionLabelPlaceholder:'".$this->enhancedTranlate('k4Cfrm-optionLabelPlaceholder' )."',"
. "optionValuePlaceholder:'".$this->enhancedTranlate('k4Cfrm-optionValuePlaceholder' )."',"
. "optionEmpty:'".$this->enhancedTranlate('k4Cfrm-optionEmpty' )."',"
. "paragraph:'".$this->enhancedTranlate('k4Cfrm-paragraph' )."',"
. "placeholder:'".$this->enhancedTranlate('k4Cfrm-placeholder' )."',"
. "preview:'".$this->enhancedTranlate('k4Cfrm-preview' )."',"
. "radioGroup:'".$this->enhancedTranlate('k4Cfrm-radioGroup' )."',"
. "radio:'".$this->enhancedTranlate('k4Cfrm-radio' )."',"
. "removeMessage:'".$this->enhancedTranlate('k4Cfrm-removeMessage' )."',"
. "remove:'".$this->enhancedTranlate('k4Cfrm-remove' )."',"
. "required:'".$this->enhancedTranlate('k4Cfrm-required' )."',"
. "richText:'".$this->enhancedTranlate('k4Cfrm-richText' )."',"
. "roles:'".$this->enhancedTranlate('k4Cfrm-roles' )."',"
. "save:'".$this->enhancedTranlate('k4Cfrm-save' )."',"
. "selectOptions:'".$this->enhancedTranlate('k4Cfrm-selectOptions' )."',"
. "select:'".$this->enhancedTranlate('k4Cfrm-select' )."',"
. "selectColor:'".$this->enhancedTranlate('k4Cfrm-selectColor' )."',"
. "selectionsMessage:'".$this->enhancedTranlate('k4Cfrm-selectionsMessage' )."',"
. "style:'".$this->enhancedTranlate('k4Cfrm-style' )."',"
. "subtype:'".$this->enhancedTranlate('k4Cfrm-subtype' )."',"
. "text:'".$this->enhancedTranlate('k4Cfrm-text' )."',"
. "textArea:'".$this->enhancedTranlate('k4Cfrm-textArea' )."',"
. "toggle:'".$this->enhancedTranlate('k4Cfrm-toggle' )."',"
. "warning:'".$this->enhancedTranlate('k4Cfrm-warning' )."',"
. "viewXML:'".$this->enhancedTranlate('k4Cfrm-viewXML' )."',"
. "yes:'".$this->enhancedTranlate('k4Cfrm-yes' )."'}}"); 
        
        
        
        
        
        
        craft()->templates->includeJs("jQuery(document.getElementById('". $namespacedId ."k4Cfrm-frm')).formBuilder({ messages: languages['default'] || {},         notify: {      success: function(msg) {        $('.checkbox-toggle').parent().hide();     }  }, controlOrder: ['text','textarea','select','checkbox','radio-group','header','paragraph'],disableFields: ['autocomplete', 'hidden','button','checkbox-group','date','file']}); jQuery('.fld-subtype [value=h1]').parent().parent().css('display','block');$('.checkbox-toggle').parent().hide();");
        
        
        /* -- Variables to pass down to our rendered template */

        $variables = array(
            'id' => $id,
            'name' => $name,
            'namespaceId' => $namespacedId,
            'values' => $value
            );

        return craft()->templates->render('k4clientform/fields/K4Clientform_K4ClientformFieldType.twig', $variables);
    }

    /**
     * Returns the input value as it should be saved to the database.
     *
     * @param mixed $value
     * @return mixed
     */
    public function prepValueFromPost($value)
    {
        return $value;
    }

    /**
     * Prepares the field's value for use.
     *
     * @param mixed $value
     * @return mixed
     */
    public function prepValue($value)
    {
        return $value;
    }
}