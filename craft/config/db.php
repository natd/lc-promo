<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(

    '*' => array(
        'tablePrefix'   => 'craft',
        'server' => '192.168.1.2',
        'database' => 'lcpromo',
        'user' => 'root',
        'password' => 'root',
    ),

    'promo.visitlakecounty.org' => array(
        'server' => 'localhost',
        'database' => 'sid14842_promo',
        'user' => 'sid14842_promo',
        'password' => 'LCcr@ft!!',
    ),

);


